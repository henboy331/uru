// Author: Jon Maken, All Rights Reserved
// License: 3-clause BSD

package env

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

// switcher script templates
var batScript = `@ECHO OFF
REM autogenerated by uru

SET "PATH=%s"
SET "GEM_HOME=%s"
`

var ps1Script = `# autogenerated by uru

$env:PATH = "%s"
$env:GEM_HOME = "%s"
`

var bashScript = `# autogenerated by uru

export PATH=%s
`

// CreateSwitcherScript creates an environment switcher script customized to the
// type of shell calling the uru runtime.
func CreateSwitcherScript(ctx *Context, path *[]string, gemHome string) (scriptName string, err error) {
	scriptType := os.Getenv(`URU_INVOKER`)

	var script string
	switch scriptType {
	case `powershell`:
		script = ps1Script
		scriptName = "uru_lackee.ps1"
	case `batch`:
		script = batScript
		scriptName = "uru_lackee.bat"
	case `bash`:
		script = bashScript
		scriptName = "uru_lackee"
	default:
		panic("uru invoked from unknown shell (check URU_INVOKER env var)")
	}
	log.Printf("[DEBUG] switcher script: %s\n", scriptName)

	switcher := filepath.Join(ctx.Home(), scriptName)
	f, err := os.Create(switcher)
	if err != nil {
		panic(fmt.Sprintf("unable to create `%s` switcher script", switcher))
	}
	defer f.Close()

	if runtime.GOOS != `windows` {
		f.Chmod(0755)
	}

	// modify the bash script to suppress GEM_HOME creation when nonexistent
	var content string
	if scriptType == `bash` {
		if gemHome != `` {
			script = strings.Join([]string{script, "export GEM_HOME=%s\n"}, ``)
			content = fmt.Sprintf(script, strings.Join(*path, string(os.PathListSeparator)), gemHome)
		} else {
			script = strings.Join([]string{script, "unset GEM_HOME\n"}, ``)

			// morph PATH on bash-on-windows environments to *nix style
			sep := string(os.PathListSeparator)
			if runtime.GOOS == `windows` {
				sep = `:`
				*path = winPathToNix(path)
			}
			content = fmt.Sprintf(script, strings.Join(*path, sep))
		}
	} else {
		content = fmt.Sprintf(script, strings.Join(*path, string(os.PathListSeparator)), gemHome)
	}

	_, err = f.WriteString(content)
	if err != nil {
		panic(fmt.Sprintf("failed to write `%s` switcher script", switcher))
	}

	return
}

// winPathToNix converts a slice of Windows formatted absolute file system
// path strings to a slice of *nix style path strings usable by cygwin
// based shells such as msysgit and msys2 bash on Windows systems.
//
// Windows 8.3 aliases, aka files with `~` in components of the name too long to
// comply legacy DOS 8.3 rules, are not supported. All modern Windows systems
// should be able to correctly handle long file names without the need to interface
// with `GetLongPathNameW` from kernel32.dll. Ensure your PATH contains only long
// path names, not legacy DOS 8.3 short path names.
//
// As the generated paths are not created with a /cygdrive prefix, you may
// need to change cygwin PATH behavior by modifying your cygwin environment's
// /etc/fstab similar to:
//
//     none / cygdrive binary,posix=0,noacl,user 0 0
//
// Example conversion: C:\some\arbitrary\path -> /C/some/arbitrary/path
func winPathToNix(path *[]string) (nix []string) {
	replacements := []string{"\\", "/", "(", "\\(", ")", "\\)", " ", "\\ "}

	for _, p := range *path {
		// pass the Canary on through untouched
		if p == Canary {
			nix = append(nix, Canary)
			continue
		}

		// morph and escape problematic chars for bash
		r := strings.NewReplacer(replacements...)

		// morph the Windows volume designator to the format cygwin understands
		parts := strings.Split(r.Replace(p), ":")
		if len(parts) == 1 {
			nix = append(nix, parts[0])
			continue
		}
		parts[0] = fmt.Sprintf("/%s", parts[0])

		nix = append(nix, strings.Join(parts, ``))
	}

	return
}
